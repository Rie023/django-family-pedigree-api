from django.apps import AppConfig


class FamilyMembersConfig(AppConfig):
    name = 'family_members'
