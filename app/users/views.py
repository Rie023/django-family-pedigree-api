from django.contrib.auth.models import User
from django.contrib.auth.password_validation import validate_password
from django.core.exceptions import ValidationError
from rest_framework.decorators import action
from rest_framework.response import Response
from rest_framework.status import HTTP_200_OK, HTTP_400_BAD_REQUEST, HTTP_404_NOT_FOUND
from rest_framework.viewsets import ViewSet

from .serializers import UserSerializer
from family_members.models import FamilyMember

class UserViewSet(ViewSet):
    serializer_class = UserSerializer

    @action(methods=['POST'], detail=False)
    def get_user(self, request, *args, **kwargs):
        data = request.data
        username = data.get('username')
        password = data.get('password')

        user = User.objects.filter(
            username=username
        ).order_by("pk").first()

        if user:
            if user.check_password(password):
                serializer = self.serializer_class(user)
                return Response(serializer.data, status=HTTP_200_OK)
            else:
                return Response({
                    'status': HTTP_400_BAD_REQUEST,
                    "message": "Password is incorrect.",
                }, status=HTTP_400_BAD_REQUEST)
        else:
            return Response({
                'status': HTTP_404_NOT_FOUND,
                "message": "User does not exist",
            }, status=HTTP_404_NOT_FOUND)

    @action(methods=['POST'], detail=False)
    def create_user(self, request, *args, **kwargs):
        data = request.data
        username = data.get('username')
        email = data.get('email')
        password = data.get('password')
        first_name = data.get('first_name')
        last_name = data.get('last_name')

        user = User.objects.filter(username=username).first()

        if user:
            return Response({
                'status': HTTP_400_BAD_REQUEST,
                'message': 'User already exists.',
            }, status=HTTP_400_BAD_REQUEST)

        else:
            user = User(username=username)
            member = FamilyMember()
            member.user = user

            try:
                validate_password(password, user)
                user.set_password(password)
                user.first_name = first_name
                user.last_name = last_name
                user.email = email
                user.save()

                member.relationship = 1
                member.relationship_count = ""
                member.first_name = first_name
                member.last_name = last_name
                member.vital_status = 1
                member.save()

            except ValidationError as e:
                return Response({
                    'status': HTTP_400_BAD_REQUEST,
                    'message': e.messages,
                }, status=HTTP_400_BAD_REQUEST)

            if user:
                serializer = self.serializer_class(user)
                return Response(serializer.data, status=HTTP_200_OK)

        return Response({
            'status': HTTP_400_BAD_REQUEST,
            'message': 'Failed to create User',
        }, status=HTTP_400_BAD_REQUEST)

    @action(methods=['POST'], detail=False)
    def edit_user(self, request, *args, **kwargs):
        data = request.data
        user_id = data.get('user_id')
        email = data.get('email')
        first_name = data.get('first_name')
        last_name = data.get('last_name')

        user = User.objects.get(pk=user_id)
        member = FamilyMember.objects.get(user=user, relationship=1)
        try:
            user.first_name = first_name
            user.last_name = last_name
            user.email = email
            user.save()

            member.first_name = first_name
            member.last_name = last_name
            member.save()

            serializer = self.serializer_class(user)
            return Response(serializer.data, status=HTTP_200_OK)

        except ValidationError as e:
            return Response({
                'status': HTTP_400_BAD_REQUEST,
                'message': e.messages,
            }, status=HTTP_400_BAD_REQUEST)

        return Response({
            'status': HTTP_400_BAD_REQUEST,
            'message': 'Failed to update User',
        }, status=HTTP_400_BAD_REQUEST)

    @action(methods=['POST'], detail=False)
    def edit_password(self, request, *args, **kwargs):
        data = request.data
        user_id = data.get('user_id')
        password1 = data.get('password1')
        password2 = data.get('password2')

        user = User.objects.get(pk=user_id)
        if password1 == password2 and len(password1) >= 8:
            try:
                user.set_password(password1)
                user.save()

                serializer = self.serializer_class(user)
                return Response(serializer.data, status=HTTP_200_OK)

            except ValidationError as e:
                return Response({
                    'status': HTTP_400_BAD_REQUEST,
                    'message': e.messages,
                }, status=HTTP_400_BAD_REQUEST)
        else:
            return Response({
            'status': HTTP_400_BAD_REQUEST,
            'message': 'Invalid passwords',
        }, status=HTTP_400_BAD_REQUEST)

        return Response({
            'status': HTTP_400_BAD_REQUEST,
            'message': 'Failed to update User',
        }, status=HTTP_400_BAD_REQUEST)