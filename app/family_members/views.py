from django.contrib.auth.models import User
from django.contrib.auth.password_validation import validate_password
from django.core.exceptions import ValidationError
from rest_framework.decorators import action
from rest_framework.response import Response
from rest_framework.status import HTTP_200_OK, HTTP_400_BAD_REQUEST, HTTP_404_NOT_FOUND
from rest_framework.viewsets import ViewSet

from .serializers import FamilyMemberSerializer
from .models import FamilyMember


class FamilyMembersViewSet(ViewSet):
    serializer_class = FamilyMemberSerializer

    @action(methods=['POST'], detail=False)
    def get_family_members(self, request):
        data = request.data
        user_id = data.get('user_id')
        user = User.objects.get(pk=user_id)

        members = FamilyMember.objects.filter(
            user=user).order_by('relationship')

        if members:
            serializer = self.serializer_class(members, many=True)
            return Response(serializer.data, status=HTTP_200_OK)
        else:
            return Response({
                'status': HTTP_404_NOT_FOUND,
                "message": "Family Members not found",
            }, status=HTTP_404_NOT_FOUND)

    @action(methods=['POST'], detail=False)
    def add_family_member(self, request):
        data = request.data
        user_id = data.get('user_id')
        user = User.objects.get(pk=user_id)

        member = FamilyMember()
        try:
            member.user = user
            member.relationship = data.get('relationship')
            str_count = str(len(FamilyMember.objects.filter(user=user, relationship=member.relationship)) + 1)
            member.relationship_count = str_count
            member.first_name = data.get('first_name')
            member.last_name = data.get('last_name')
            member.birthdate = data.get('birthdate')
            member.vital_status = data.get('vital_status')
            member.save()

        except ValidationError as e:
            return Response({
                'status': HTTP_400_BAD_REQUEST,
                'message': e.messages,
            }, status=HTTP_400_BAD_REQUEST)

        serializer = self.serializer_class(member)
        return Response(serializer.data, status=HTTP_200_OK)

    @action(methods=['POST'], detail=False)
    def edit_family_member(self, request):
        data = request.data
        user_id = data.get('user_id')
        user = User.objects.get(pk=user_id)

        member = FamilyMember.objects.get(pk=data.get('id'))
        try:
            member.first_name = data.get('first_name')
            member.last_name = data.get('last_name')
            member.birthdate = data.get('birthdate')
            member.vital_status = data.get('vital_status')
            member.save()

        except ValidationError as e:
            return Response({
                'status': HTTP_400_BAD_REQUEST,
                'message': e.messages,
            }, status=HTTP_400_BAD_REQUEST)

        serializer = self.serializer_class(member)
        return Response(serializer.data, status=HTTP_200_OK)


    @action(methods=['POST'], detail=False)
    def delete_family_member(self, request):
        data = request.data

        member = FamilyMember.objects.get(pk=data.get('id'))
        try:
            member.delete()

        except ValidationError as e:
            return Response({
                'status': HTTP_400_BAD_REQUEST,
                'message': e.messages,
            }, status=HTTP_400_BAD_REQUEST)

        serializer = self.serializer_class(member)
        return Response(serializer.data, status=HTTP_200_OK)
