from django.db import models
from django.contrib.auth.models import User
from django.forms import ModelForm

class FamilyMember(models.Model):
    id = models.AutoField(primary_key=True)
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name='family_user')
    relationship = models.IntegerField()
    relationship_count = models.CharField(max_length=7, blank=True)
    first_name = models.CharField(max_length=255)
    last_name = models.CharField(max_length=255)
    birthdate = models.DateField(null=True, blank=True)
    vital_status = models.BooleanField()