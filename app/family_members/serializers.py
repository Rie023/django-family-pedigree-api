from rest_framework import serializers

from .models import FamilyMember

class FamilyMemberSerializer(serializers.ModelSerializer):
    class Meta:
        model = FamilyMember
        fields = [
            'id',
            'user',
            'relationship',
            'relationship_count',
            'first_name',
            'last_name',
            'birthdate',
            'vital_status',
        ]